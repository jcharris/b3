help:
	@echo "Please read the README file in the root directory."
clean:
	find . -name "*~" -o -name "*.o" -o -name "*.mod" | xargs rm -f
cleandat:
	find . -name "*.dat" | xargs rm -f
cleanlib:
	find . -name "*.a" -o -name "*.so" | xargs rm -f
cleanall:
	make clean
	make cleandat
	make cleanlib
commit  :
	hg commit
	hg push
	hg pull -u
save    :
	make cleanall
	cd .. && tar zcvf b3.tgz b3
revert	:
	hg revert --all
	find . -name "*.orig" | xargs rm
