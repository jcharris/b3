#ifndef b3_h
#define b3_h

#include "solver.h"

typedef Eigen::SparseLU
<Eigen::SparseMatrix<real_t,Eigen::ColMajor>,
 Eigen::COLAMDOrdering<int> > MP;                               // Preconditioner type

class B3 : public Solver {
private:
  std::vector<Eigen::Triplet<real_t> > XCT;                     // Triplets 
  Eigen::SparseMatrix<real_t> XC;                               // Sparse 
  Eigen::BiCGSTAB<Eigen::SparseMatrix<real_t> > solver;         // Solver (zero-deriv)
  std::vector<Eigen::Triplet<real_t> > eXCT;                    // Triplets 
  Eigen::SparseMatrix<real_t> eXC;                              // Sparse
  Eigen::BiCGSTAB<Eigen::SparseMatrix<real_t> > esolver;        // Solver (extrap)
  MP mp;                                                        // Preconditioner
  eVect etat0, phit0;                                           // 
public:
  int P;                                                        // Maximum order
  std::vector<eVect> phi;                                       // Coefficients
  std::vector<eVect> phi_c;                                     // Control points
  eVect eta;                                                    // Free surface elevation
  eVect phi_fs;                                                 // Free surface potential
  eVect h;                                                      // Seabed elevation
  eVect ht;                                                     // Bed vertical velocity
  real_t dx;                                                    // Grid spacing
  real_t g;                                                     // Gravity
  int N;                                                        // Number of points
  int order;                                                    // Order of expansion
private:
  //! Compute derivative
  eVect fx(eVect & f_c) {
    eVect res(N);                                               // Result
    for (int i=0; i<N; i++) {                                   // Loop over points
      res[i] = (f_c[i+2] - f_c[i])/2.0;                         //  Approx first deriv
    }                                                           // End loop
    return res;                                                 // Return result
  }
  
  //! Compute second derivative
  eVect fxx(eVect & f_c) {
    eVect res(N);                                               // Result
    for (int i=0; i<N; i++) {                                   // Loop over points
      res[i] = f_c[i+2] - 2.0*f_c[i+1] + f_c[i];                //  Approx second deriv
    }                                                           // End loop
    return res;                                                 // Return result
  }
  
  //! Get control points
  eVect ctrl(eVect & f) {
    eVect f_in(N+2);                                            // Declare result
    f_in[0] = 0;                                                // Zero derivative
    for (int i=1; i<N+1; i++) {                                 // Loop over points
      f_in[i] = f[i-1];                                         //  Store solution
    }                                                           // End loop
    f_in[N+1] = 0;                                              // Zero derivative
    return solver.solve(f_in);                                  // Return result
  }

  //! Get control points
  eVect ectrl(eVect & f) {
    eVect f_in(N+2);                                            // Declare result
    f_in[0] = 0;                                                // Zero third deriv
    for (int i=1; i<N+1; i++) {                                 // Loop over points
      f_in[i] = f[i-1];                                         //  Store solution
    }                                                           // End loop
    f_in[N+1] = 0;                                              // Zero third deriv
    return esolver.solve(f_in);                                 // Return result
  }

  //! Compute horizontal velocity at an elevation zref
  eVect us(eVect & zref) {
    eVect u_ref(N);

    eVect myphix(N);
    myphix = fx(phi_c[P])/dx;
    for (int i=0; i<N; i++) { // Horner's method
      u_ref[i]=myphix[i];
    }
    for (int q=P-1; q>=0; q--) {
      myphix = fx(phi_c[q])/dx;
      for (int i=0; i<N; i++) {
	u_ref[i]=u_ref[i]*zref[i]+myphix[i];
      }
    }

    /*
    for (int i=0; i<N; i++) {
      u_ref[i]=0.0;
    }
    for (int q=0; q<=P; q++) {
      eVect myphix(N);
      myphix = fx(phi_c[q])/dx;
      for (int i=0; i<N; i++) {
	u_ref[i] += myphix[i]*pow(zref[i],q);
      }
    }
    */
    return u_ref;                                               // Return result
  }
  
  //! Compute vertical velocity at an elevation zref
  eVect vs(eVect & zref) {
    eVect v_ref(N);

    for (int i=0; i<N; i++) { // Horner's method
      v_ref[i]=phi[P][i]*real_t(P);
    }
    for (int q=P-1; q>=1; q--) {
      for (int i=0; i<N; i++) {
	v_ref[i]=v_ref[i]*zref[i]+phi[q][i]*real_t(q);
      }
    }

    /*
    for (int i=0; i<N; i++) {
      v_ref[i]=0.0;
    }
    for (int q=1; q<=P; q++) {
      eVect myphi = phi[q];
      for (int i=0; i<N; i++) {
	v_ref[i] += myphi[i]*pow(zref[i],q-1)*real_t(q);
      }
    }
    */
    return v_ref;                                               // Return result
  }
  
public:
  //! Preconditioner
  eVect precond(const eVect & x) {
    return mp.solve(x);                                         // Solve preconditioner
    //return x;
  }

  //! Update potential coefficients
  void Update() {
    for (int q=0; q<=P; q++) {
      if (q>=2) {
	phi[q] = -fxx(phi_c[q-2])/real_t(q)/real_t(q-1)/(dx*dx);//
      }
      phi_c[q] = ctrl(phi[q]);
    }
  }
  
  //! Compute free-surface velocity potential
  eVect matvec(const eVect & x) {
    for (int i=0; i<N; i++) {
      phi[0][i] = x[2*i+0];
      phi[1][i] = x[2*i+1];
    }
    Update();
    eVect phi_fs(N), ht(N);

    for (int i=0; i<N; i++) {
      phi_fs[i]=phi[P][i];
    }
    for (int q=P-1; q>=0; q--) {
      for (int i=0; i<N; i++) {
	phi_fs[i]=phi_fs[i]*eta[i]+phi[q][i];
      }
    }
    
    /*
    for (int i=0; i<N; i++) {
      phi_fs[i]=0.0;
    }
    for (int q=0; q<=P; q++) {
      eVect myphi = phi[q];
      for (int i=0; i<N; i++) {
	phi_fs[i] += myphi[i]*pow(eta[i],q);
      }
    }
    */
    eVect u_h = us(h);                                     //  Get horizontal velocity
    eVect v_h = vs(h);                                     //  Get vertical velocity
    eVect h_c = ctrl(h);                                        //  Get control points
    eVect hx = fx(h_c)/(dx);                                    //  Get free-surface slope
    for (int i=0; i<N; i++) {                                 //  Loop over free-surface
      ht[i] = -hx[i]*u_h[i]+v_h[i];                     //   Get eta_t
    }
    eVect y(2*N);
    for (int i=0; i<N; i++) {                                 //  Loop over points
      y[2*i+0] = phi_fs[i];
      y[2*i+1] = ht[i];
    }
    return y;
  }

  //! Setup preconditioner
  void Setup () {
    //    g = 1.0;                                                    // Fix gravity
    g = 9.81;

    phi.resize(P+1);
    phi_c.resize(P+1);
    for (int q=0; q<=P; q++) {
      phi[q].resize(N);
      phi_c[q].resize(N+2);
    }
    etat0.resize(N);
    phit0.resize(N);

    XCT.reserve(3*(N+2));
    eXCT.reserve(3*(N+2));
    eXCT.push_back(Eigen::Triplet<real_t>(0,0, 1.));
    eXCT.push_back(Eigen::Triplet<real_t>(0,1,-3.));
    eXCT.push_back(Eigen::Triplet<real_t>(0,2, 3.));
    eXCT.push_back(Eigen::Triplet<real_t>(0,3,-1.));
    eXCT.push_back(Eigen::Triplet<real_t>(N+1,N-2, 1.));
    eXCT.push_back(Eigen::Triplet<real_t>(N+1,N-1,-3.));
    eXCT.push_back(Eigen::Triplet<real_t>(N+1,N  , 3.));
    eXCT.push_back(Eigen::Triplet<real_t>(N+1,N+1,-1.));
    
    XCT.push_back(Eigen::Triplet<real_t>(0,0,-1.));
    XCT.push_back(Eigen::Triplet<real_t>(0,2, 1.));
    XCT.push_back(Eigen::Triplet<real_t>(N+1,N-1,-1.));
    XCT.push_back(Eigen::Triplet<real_t>(N+1,N+1,1.));
    for (int i=1; i<N+1; i++) {
      XCT.push_back(Eigen::Triplet<real_t>(i,i-1, 1./6.));
      XCT.push_back(Eigen::Triplet<real_t>(i,i  , 4./6.));
      XCT.push_back(Eigen::Triplet<real_t>(i,i+1, 1./6.));
      eXCT.push_back(Eigen::Triplet<real_t>(i,i-1, 1./6.));
      eXCT.push_back(Eigen::Triplet<real_t>(i,i  , 4./6.));
      eXCT.push_back(Eigen::Triplet<real_t>(i,i+1, 1./6.));
    }
    XC.resize(N+2,N+2);                                   // Resize 
    XC.setFromTriplets(XCT.begin(),XCT.end());                  // Assemble matrix
    solver.compute(XC);                                         // Prepare 
    eXC.resize(N+2,N+2);                                   // Resize 
    eXC.setFromTriplets(eXCT.begin(),eXCT.end());                  // Assemble matrix
    esolver.compute(eXC);                                         // Prepare 

    eVect eta0 = eta;                                           // Save eta
    eta = eVect::Zero(N);                                       // Set linear condition
    eVect f0 = eVect::Zero(2*N);                                // Create test vector
    eMat M(2*N,2*N);                                            // Dense preconditioner
    for (int i=0; i<2*N; i++) {                                 // Loop over nodes
      f0(i)=1;                                                  //  Set value
      M.col(i) = matvec(f0);                                    //  Compute column
      f0(i)=0;                                                  //  Reset value
    }                                                           // End loop
    Eigen::SparseMatrix<real_t,Eigen::ColMajor> MS(2*N,2*N);    // Declare matrix
    MS = M.sparseView(1e-9);                                    // Save sparse version
    //std::ofstream msout;
    //msout.open("precond.txt");
    //msout << M << std::endl;
    //msout.close();
    MS.makeCompressed();                                        // Compress sparse matrix
    mp.analyzePattern(MS);                                      // Analyze structure
    mp.factorize(MS);                                           // Construct ILU factorized
    eta = eta0;                                                 // Replace free-surface
  }

  //! Constructor
  B3(real_t tol = 1e-12,
     int maxIters = 1000,
     int verbose = 0,
     int solver = 1,
     int restart = 1000) {
    Solver::tol = tol;
    Solver::maxIters = maxIters;
    Solver::verbose = verbose;
    Solver::solver = solver;
    Solver::restart = restart;
#if VERBOSE
    std::cout << "-------------" << std::endl;
    std::cout << "B3 simulation" << std::endl;
    std::cout << "-------------" << std::endl;
#endif
    N = 0;
    P = 9;
  };

  //! Destructor
  ~B3() {};
  
  //! Apply timestep
  void RK3(real_t dt) {
    eVect eta0(N), phi_fs0(N);                                  // Initial state
    eVect eta_c(N+2), etax(N);                                  // Derivative of eta
    eVect etat(N), phit(N);                                     // Temporary derivatives
    real_t A[3]={0.0,3.0/4.0,1.0/3.0};                          // Set coefficients for RK3
    real_t B[3]={1.0,1.0/4.0,2.0/3.0};                          // Set coefficients for RK3
    eta0 = eta;                                                 // Save eta
    phi_fs0 = phi_fs;                                           // Save phi_fs
    for (int istage=0; istage<3; istage++) {                    // Loop over RK3
      if (istage==0) { // if first stage
	etat0 = etat; // save original
	phit0 = phit;
      }
      eVect x(2*N),rhs(2*N);                                    //  Vector
      for (int i=0; i<N; i++) {                                 //  Loop over points
	x[2*i+0] = phi[0][i];                                     //  
	x[2*i+1] = phi[1][i];
	rhs[2*i+0] = phi_fs[i];
	rhs[2*i+1] = ht[i];
      }
      solve(x,rhs);                                             //  Solve Laplace
      for (int i=0; i<N; i++) {                                 //  Loop over points
	phi[0][i] = x[2*i+0];                                     //   Extract phi0
	phi[1][i] = x[2*i+1];                                     //   Extract phi1
      }                                                         //  End loop
      Update();                                                 //  Update state
      eVect u_fs = us(eta);                                     //  Get horizontal velocity
      eVect v_fs = vs(eta);                                     //  Get vertical velocity
      eta_c = ctrl(eta);                                        //  Get control points
      etax = fx(eta_c)/(dx);                                    //  Get free-surface slope
      for (int i=0; i<N; i++) {                                 //  Loop over free-surface
	etat[i] = -etax[i]*u_fs[i]+v_fs[i];                     //   Get eta_t
	phit[i] = -g*eta[i] - 0.5*u_fs[i]*u_fs[i]
	  - 0.5*v_fs[i]*v_fs[i] + etat[i]*v_fs[i];              //   Get phi_t
        eta[i] = A[istage]*eta0[i]
          + B[istage]*(eta[i]+dt*(etat[i]));                    //   Update eta
        phi_fs[i] = A[istage]*phi_fs0[i]
          + B[istage]*(phi_fs[i]+dt*(phit[i]));                 //   Update phi
      }                                                         //  End loop
    }                                                           // End loop
  }

  //! Apply timestep
  void ABM2(real_t dt) {
    eVect eta_c(N+2), etax(N);                                  // Derivative of eta
    eVect etat(N), phit(N);                                     // Temporary derivatives
    eVect x(2*N),rhs(2*N);                                    //  Vector
    for (int i=0; i<N; i++) {                                 //  Loop over points
      x[2*i+0] = phi[0][i];                                     //  
      x[2*i+1] = phi[1][i];
      rhs[2*i+0] = phi_fs[i];
      rhs[2*i+1] = ht[i];
    }
    solve(x,rhs);                                             //  Solve Laplace
    for (int i=0; i<N; i++) {                                 //  Loop over points
      phi[0][i] = x[2*i+0];                                     //   Extract phi0
      phi[1][i] = x[2*i+1];                                     //   Extract phi1
    }                                                         //  End loop
    Update();                                                 //  Update state
    eVect u_fs = us(eta);                                     //  Get horizontal velocity
    eVect v_fs = vs(eta);                                     //  Get vertical velocity
    eta_c = ctrl(eta);                                        //  Get control points
    etax = fx(eta_c)/(dx);                                    //  Get free-surface slope
    for (int i=0; i<N; i++) {                                 //  Loop over free-surface
      etat[i] = -etax[i]*u_fs[i]+v_fs[i];                     //   Get eta_t
      phit[i] = -g*eta[i] - 0.5*u_fs[i]*u_fs[i]
	- 0.5*v_fs[i]*v_fs[i] + etat[i]*v_fs[i];              //   Get phi_t
    }                                                         //  End loop
    eta = eta + dt*(1.5*etat - 0.5*etat0);
    phi_fs = phi_fs + dt*(1.5*phit - 0.5*phit0);
    etat0 = etat; // save old
    phit0 = phit;
  }
};

#endif
