#ifndef solver_h
#define solver_h

// consider #define EIGEN_NO_DEBUG
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <Eigen/Jacobi>
// macro count in traversal an issue, assert issues?

#include "logger.h"

//typedef float real_t;
typedef double real_t;
//typedef long double real_t;
typedef Eigen::Matrix<real_t,Eigen::Dynamic,1> eVect;           // Vector type
typedef Eigen::Matrix<real_t,Eigen::Dynamic,Eigen::Dynamic> eMat;

//! Class definition
class Solver {
private:
public:
  int verbose;                                                  // 0/1/2/3 : silent/info/lots/debug
  int solver;                                                   // 0 - BiCGSTAB; 1 - GMRES
  real_t tol;                                                   // Tolerance
  int maxIters;                                                 // Maximum iterations
  int restart;                                                  // Restart for GMRES

private:
  //! Apply preconditioner
  virtual eVect precond(const eVect & x) = 0;
  
  //! Matrix-vector multiplication for system matrix
  virtual eVect matvec(const eVect & x) = 0;

  //! Solve linear system of equations
  void BiCGSTAB(eVect & x, eVect & b) {
    int node_N = x.size();                                      // Fix number of nodes
    eVect r = b - matvec(x);                                    // Get residual (or r=BEM(Phi,Phin)?)
    eVect r0 = r;                                               // Set starting residual
    real_t rhs_sqnorm = b.squaredNorm();                        // Get norm of rhs
    real_t rho   = 1;                                           // 
    real_t alpha = 1;
    real_t omega = 1;
    int iter = 0;                                               // Initialize iteration counter
    eVect v = eVect::Zero(node_N);
    eVect p = eVect::Zero(node_N);
    while ((r.squaredNorm()/rhs_sqnorm > tol*tol)
           && (iter<maxIters)) {                                // While solution is not converged
      real_t rho_old = rho;                                     //  Set 
      rho = r0.dot(r);                                          //  Get 
      real_t beta = (rho/rho_old) * (alpha / omega);            //
      p = r + beta * (p - omega * v);                           //
      eVect y = precond(p);                                     //  Apply preconditioner
      v = matvec(y);                                            //  Apply system matrix
      alpha = rho / r0.dot(v);                                  //
      eVect s = r - alpha * v;                                  //
      eVect z = precond(s);                                     //  Apply preconditioner
      eVect t = matvec(z);                                      //  Apply system matrix
      omega = t.dot(s) / t.squaredNorm();                       //  
      x += alpha * y + omega * z;                               //  Update solution vector
      r = s - omega * t;                                        //  Update residual
      ++iter;                                                   //  Increment iteration counter
      if (verbose==3) {                                         //  If verbose is 3
	  std::cout << "iter = "
		    << iter << " : "
		    << sqrt(r.squaredNorm()/rhs_sqnorm)
		    << "          \r" << std::flush;            //    Update error printout
      }                                                         //  End if
    }                                                           // End loop
    if ((verbose==1) || (verbose==2)) {                         // If verbose is 1 or 2
	std::cout << "iter = " << iter << " : "
		  << sqrt(r.squaredNorm()/rhs_sqnorm)
		  << std::endl;                                 //   Display result
    }                                                           // End if
  }

  //! Solver
  void GMRES(eVect & x, eVect & b) {
    int node_N = x.size();
    int iter = 0;
    eVect p0 = b - matvec(x);
    eVect r0 = precond(p0);
    real_t r0Norm = r0.norm();
    if(r0Norm == 0) {   // is initial guess already good enough?
      return;
    }
    eMat H = eMat::Zero(node_N, restart + 1);
    eVect w = eVect::Zero(restart + 1);
    eVect tau = eVect::Zero(restart + 1);
    std::vector<Eigen::JacobiRotation<real_t> > G(restart);
    eVect t(node_N), v(node_N), workspace(node_N), x_new(node_N);
    Eigen::Ref<eVect> H0_tail = H.col(0).tail(node_N - 1);
    real_t beta;
    r0.makeHouseholder(H0_tail, tau.coeffRef(0), beta);
    w(0) = beta;
    int k;
    for (k=1; k<=restart; ++k) {
      ++iter;
      v = eVect::Unit(node_N, k - 1);
      for (int i=k-1; i>=0; --i) {
	v.tail(node_N - i).applyHouseholderOnTheLeft(H.col(i).tail(node_N - i - 1), tau.coeffRef(i), workspace.data());
      }
      t.noalias() = matvec(v);    // apply matrix M to v:  v = mat * v;
      v = precond(t);
      for (int i = 0; i < k; ++i) {
	v.tail(node_N - i).applyHouseholderOnTheLeft(H.col(i).tail(node_N - i - 1), tau.coeffRef(i), workspace.data());
      }
      if (v.tail(node_N - k).norm() != 0.0) {
	if (k <= restart) {
	  Eigen::Ref<eVect> Hk_tail = H.col(k).tail(node_N - k - 1);
	  v.tail(node_N - k).makeHouseholder(Hk_tail, tau.coeffRef(k), beta);
	  v.tail(node_N - k).applyHouseholderOnTheLeft(Hk_tail, tau.coeffRef(k), workspace.data());
	}
      }
      if (k > 1) {
	for (int i = 0; i < k - 1; ++i) {
	  v.applyOnTheLeft(i, i + 1, G[i].adjoint());
	}
      }
      if (k<node_N && v(k) != 0) {
	G[k - 1].makeGivens(v(k - 1), v(k));
	v.applyOnTheLeft(k - 1, k, G[k - 1].adjoint());
	w.applyOnTheLeft(k - 1, k, G[k - 1].adjoint());
      }
      H.col(k-1).head(k) = v.head(k);
      if (verbose==3) {                                         //  If verbose is 3
          std::cout << "iter = "
		    << iter << " : "
		    << std::abs(w(k))/r0Norm
		    << "          \r" << std::flush;            //   Update error printout
      }
      bool stop = (k==node_N || std::abs(w(k)) < tol * r0Norm || iter == maxIters);
      if (stop || k == restart) {
	Eigen::Ref<eVect> y = w.head(k);
	H.topLeftCorner(k, k).triangularView <Eigen::Upper>().solveInPlace(y);
	x_new.setZero();
	for (int i=k-1; i>=0; --i) {
	  x_new(i) += y(i);
	  x_new.tail(node_N - i).applyHouseholderOnTheLeft(H.col(i).tail(node_N - i - 1), tau.coeffRef(i), workspace.data());
	}
	x += x_new;
	if (stop) {
	  if (verbose==3) {                                         //  If verbose is 3
	      std::cout << std::endl;                                   // 
	  }

    if ((verbose==1) || (verbose==2)) {                         // If verbose is 1 or 2
	std::cout << "iter = " << iter << " : "
		  << std::abs(w(k))/r0Norm
		  << std::endl;                                 //   Display result
    }                                                           // End if

	  
	  return;
	} else {
	  k=0;
	  p0.noalias() = b - matvec(x);
	  r0 = precond(p0);
	  H.setZero();
	  w.setZero();
	  tau.setZero();
	  r0.makeHouseholder(H0_tail, tau.coeffRef(0), beta);
	  w(0) = beta;
	}
      }
    }
    if (verbose==3) {                                         //  If verbose is 3
	std::cout << std::endl;                                   // 
    }
    if ((verbose==1) || (verbose==2)) {                         // If verbose is 1 or 2
	std::cout << "iter = " << iter << " : "
		  << std::abs(w(k))/r0Norm
		  << std::endl;                                 //   Display result
    }                                                           // End if
    return;
  }
  
public:
  //! Constructor
  Solver(real_t tol = 1e-9,
	 int maxIters = 100,
	 int verbose = 0,
	 int solver = 0,
	 int restart = 100) {
  }

  //! Solve Laplace problem
  void solve (eVect & x0, eVect & rhs) {
    logger::startTimer("Total Solve");                         // Start timer
    if (solver==0)                                             // If
      BiCGSTAB(x0,rhs);                                        //  Use BiCGSTAB
    else if (solver==1)                                        // Else if 
      GMRES(x0,rhs);                                           //  Use GMRES
    else                                                       // Else
      std::cout << "ERROR" << std::endl;                       //  Replace with exit
    logger::stopTimer("Total Solve");                          // Stop timer
    if (verbose>=2) {                                          // If verbose 2 or 3
	logger::verbose = 1;                                   //   Turn on messages
    }                                                          // End if
    logger::printTime("Total Solve");                          // Print timer
    logger::resetTimer("Total Solve");                         // Reset timer
    logger::verbose = 0;                                       // Turn off messages
  }

  //! Destructor
  ~Solver() {
  }
};

#endif
