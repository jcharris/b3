// Demo program for the solution of linear system of equations

#include <getopt.h>
#include "solver.h"

class LinearSystem : public Solver {
public:
  LinearSystem(real_t tol = 1e-9,
	       int maxIters = 100,
	       int verbose = 0,
	       int solver = 1,
	       int restart = 100) {
    Solver::tol = tol;                                          // Copy tolerance
    Solver::maxIters = maxIters;                                // Copy iterations
    Solver::verbose = verbose;                                  // Copy verbosity
    Solver::solver = solver;                                    // Copy solver
    Solver::restart = restart;                                  // Copy restarts
  }

  //! Preconditioner
  eVect precond(const eVect & x) {
    return x;                                                   // Identity
  }
  
  //! Matrix-vector product
  eVect matvec(const eVect & x) {
    eVect y(4);                                                 // Declare result
    y(0) = 2*x(0) + x(1);                                       // Fake operator
    y(1) = 2*x(1) + x(2);
    y(2) = 2*x(2) + x(3);
    y(3) = 2*x(3) + x(2);
    return y;                                                   // Return result
  }
};

int verbose=1;                                                  // Level of info

static struct option long_options[] = {
  {"verbose",      1, 0, 'v'},
  {"help",         0, 0, 'h'},
  {0, 0, 0, 0}
};

//! Usage statement
void usage(char * name) {
  fprintf(stderr,
	  "Usage: %s [options]\n"
	  "Long option (short option)     : Description (Default value)\n"
	  " --verbose (-v) [0/1/2/3]      : Print information to screen (%d)\n"
	  " --help (-h)                   : Show this help document\n",
	  name,
	  verbose);
}

//! Main routine
int main(int argc, char ** argv) {
  LinearSystem system;                                          // Declare system
  while (1) {                                                   // Loop over arguments
    int option_index;                                           //  Declare index
    int c = getopt_long(argc, argv, "v:h",
			long_options, &option_index);           //  Get options
    if (c==-1) break;                                           //  If none, end
    switch (c) {                                                //  Switch between options
    case 'v':                                                   //  Case for verbose
      verbose = atoi(optarg);                                   //   Get value
      break;                                                    //   End case
    case 'h':                                                   //  Case for help
      usage(argv[0]);                                           //    Display info
      exit(0);                                                  //   Exit program
    default:                                                    //  Case for blank
      usage(argv[0]);                                           //    Display info
      exit(0);                                                  //   Exit program
    }                                                           //  End switch
  }                                                             // End loop
  bool pass = true;                                             // Assume pass
  if (verbose>=2) {                                             //  If high verbosity
    std::cout << "Linear solver:" << std::endl;                 //   Title
    std::cout << "--------------" << std::endl;                 //   Underline
  }                                                             //  End if
  eVect x(4);                                                   // Declare unknown
  eVect rhs(4);                                                 // Declare rhs
  x << 1, 2, 3, 4;                                              // Set initial guess
  rhs << 4, 3, 2, 1;                                            // Set rhs
  system.solve(x,rhs);                                          // Solve system
  eVect err(4);                                                 // Declare error
  err << 1.5, 1.0, 1.0, 0.0;                                    // Store theoretical
  err = err - x;                                                // Compute difference
  if (err.norm()>1e-6) {                                        // If error big
    pass=false;                                                 //  Fail test
  }                                                             // End if
  if (verbose==3) {                                             //  If very verbose
    std::cout << x << std::endl;                                //   Display result
  }                                                             //  End if
  if (verbose>0) {                                              //  If verbose
    if (pass) {                                                 //   If pass
      std::cout << "       solve: PASSED" << std::endl;         //    Display pass
    } else {                                                    //   Else
      std::cout << "       solve: FAILED" << std::endl;         //    Display fail
    }                                                           //   End if
  }                                                             //  End if
}
