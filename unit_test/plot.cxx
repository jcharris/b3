#include <mgl2/qt.h>

mglData x(50);                                                  // Some x data
mglData y(50);                                                  // Some y data

//! Window definition
int sample(mglGraph *gr) {
  gr->Box();                                                    // Box
  gr->SetRanges(0,50,-1,1);                                     // Fix ranges
  gr->Plot(x,y);                                                // Plot data
  return 0;                                                     // Return success
}

//! Main routine
int main(int argc,char **argv) {
  for(int i=0; i<50; i++) {                                     // Loop over nodes
    x.a[i] = double(i);                                         //  Fix x data
    y.a[i] = sin(double(i));                                    //  Fix y data
  }                                                             // End loop
  mglQT gr(sample,"MathGL examples");                           // Define window
  return gr.Run();                                              // Make plot
}
