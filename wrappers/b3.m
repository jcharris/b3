function [eta,phi] = b3(eta,phi,h,ht,dx,dt)
% [eta,phi] = b3(eta,phi,h,ht,dx,dt)
%
% This routine uses a local polynomial approximation to solve for fully
% nonlinear potential flow, with a representation of the velocity
% potential by a 9th-order polynomial in the vertical and cubic
% B-splines in the horizontal.  Assuming a fixed "dx", it uses
% third-order Runge-Kutta to advance in time by "dt", given the wave
% elevation "eta", free surface velocity potential "phi", the seabed
% position "h", and time-derivative of the seabed depth "ht".  The
% values returned are the wave elevation and free surface velocity
% potential at the new time.  Presently vertical walls are assumed on
% each side of the wave tank.
