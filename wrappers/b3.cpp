#include "mex.h"
#include "b3.h"

B3 b3;

void mexFunction (int nlhs, mxArray *plhs[],
		  int nrhs, const mxArray *prhs[]) {
  mxArray *phi_m, *eta_m, *h_m, *ht_m, *dx_m, *dt_m;
  mxArray *phin_m, *etan_m;

  double *phi, *eta, *h, *ht, *dx, *dt;
  double *phin, *etan;

  const mwSize *dims;                                           // Declare dimensions
  int dimx, dimy;                                               // Individual dimension

  eta_m = mxDuplicateArray(prhs[0]);
  eta = mxGetPr(eta_m);
  phi_m = mxDuplicateArray(prhs[1]);
  phi = mxGetPr(phi_m);
  h_m = mxDuplicateArray(prhs[2]);
  h = mxGetPr(h_m);
  ht_m = mxDuplicateArray(prhs[3]);
  ht = mxGetPr(ht_m);
  dx_m = mxDuplicateArray(prhs[4]);
  dx = mxGetPr(dx_m);
  dt_m = mxDuplicateArray(prhs[5]);
  dt = mxGetPr(dt_m);
  
  dims = mxGetDimensions(prhs[0]);                              // Get size of nodes
  dimy = (int)dims[0]; dimx = (int)dims[1];                     // Save output dimensions
  int N = (int)dims[0] * (int)dims[1];                     // Set number of nodes

  b3.dx = dx[0];                                                 // Grid spacing
  b3.eta.resize(N);
  b3.phi_fs.resize(N);
  b3.ht.resize(N);
  b3.h.resize(N);
  for (int i=0; i<N; i++) {                                     // Loop over nodes
    b3.eta[i] = eta[i];                                            //  Initialize eta
    b3.phi_fs[i] = phi[i];                                         //  Initialize potential
    b3.h[i] = h[i];                                              //  Initialize depth
    b3.ht[i] = ht[i];                                             //  Initialize movement
  }                                                             // End if
  if (b3.N==0) {
    b3.N = N;
    b3.Setup();
  }
  b3.RK3(dt[0]);

  dimy = N;
  dimx = 1;
  etan_m = plhs[0] = mxCreateDoubleMatrix(dimy,dimx,mxREAL);  // Associate
  etan = mxGetPr(etan_m);
  phin_m = plhs[1] = mxCreateDoubleMatrix(dimy,dimx,mxREAL);  // Associate outputs
  phin = mxGetPr(phin_m);
  for (int i=0; i<N; i++) {
    etan[i] = b3.eta[i];
    phin[i] = b3.phi_fs[i];
  }
  return;                                                       // Return from call
}
