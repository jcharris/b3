Mx = 5;                                                         % fix grid size
H = 0.5;                                                        % fix height

dx = 1/Mx;                                                      % get dx
myx=linspace(-25,675,700*Mx+1);                                 % set x
[zs,ws,fs,SWP] = SolitaryGravityAmplitude(H);                   % obtain params
myz=interp1(real(zs),imag(zs),myx,'extrap')';                   % interp eta
myphi=interp1(real(zs),real(fs),myx,'extrap')'*sqrt(9.81);      % interp phi

save('-ascii','eta.txt','myz');                                 % save eta
save('-ascii','phi.txt','myphi');                               % save phi
