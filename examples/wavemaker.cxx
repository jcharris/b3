// Demo program for the bottom-tilted wavemaker

#include <getopt.h>
#include "b3.h"

real_t maxeta=0;                                                // Maximum eta
real_t mineta=0;                                                // Minimum eta
real_t a=0.2;                                                   // Amplitude of motion
real_t b=0.35;                                                  // Rise time

#ifdef GUI
#include <mgl2/qt.h>
mglData x(400);                                                 // X-axis data
mglData y(400);                                                 // Y-axis data

//! Plot data
int sample(mglGraph *gr) {
  gr->SetRanges(0,5,mineta-0.05,maxeta+0.05);                   // Set limits
  char mytitle[26];                                             // Declare title
  sprintf(mytitle,"a = %.02f, b = %.02f",a,b);                  // Add data
  gr->Title(mytitle);                                           // Plot title
  gr->SetFontSizePT(8);                                         // Adjust size
  gr->Label('x',"t",0);                                         // X-axis label
  gr->Label('y',"\\eta",0);                                     // Y-axis label
  gr->Plot(x,y);                                                // Plot data
  gr->Axis();                                                   // Make axis
  return 0;                                                     // Return success
}
#endif

int verbose=1;                                                  // Level of info
int plot=1;                                                     // Make plot or not

static struct option long_options[] = {
  {"verbose",      1, 0, 'v'},
  {"amplitude",    1, 0, 'a'},
  {"time",         1, 0, 'b'},
  {"plot",         0, 0, 'p'},
  {"help",         0, 0, 'h'},
  {0, 0, 0, 0}
};

void usage(char * name) {
  fprintf(stderr,
          "Usage: %s [options]\n"
          "Long option (short option)     : Description (Default value)\n"
          " --verbose (-v) [0/1/2/3]      : Print information to screen (%d)\n"
          " --amplitude (-a)              : Amplitude of motion (%f)\n"
          " --time (-b)                   : Rise time (%f)\n"
          " --plot (-p) [0/1]             : Plot graph (%d)\n"
          " --help (-h)                   : Show this help document\n",
          name,
          verbose,
	  a,
	  b,
	  plot);
}

//! Main routine
int main(int argc, char ** argv) {
  while (1) {                                                   // Loop over arguments
    int option_index;                                           //  Declare index
    int c = getopt_long(argc, argv, "a:b:p:v:h",
                        long_options, &option_index);           //  Get options
    if (c==-1) break;                                           //  If none, end
    switch (c) {                                                //  Switch between options
    case 'v':                                                   //  Case for verbose
      verbose = atoi(optarg);                                   //   Get value
      break;                                                    //   End case
    case 'a':                                                   //  Case for amplitude
      a = atof(optarg);                                         //   Get value
      break;                                                    //   End case
    case 'b':                                                   //  Case for rise time
      b = atof(optarg);                                         //   Get value
      break;                                                    //   End case
    case 'p':                                                   //  Case for plot
      plot = atoi(optarg);                                      //   Get value
      break;                                                    //   End case
    case 'h':                                                   //  Case for help
      usage(argv[0]);                                           //    Display info
      exit(0);                                                  //   Exit program
    default:                                                    //  Case for blank
      usage(argv[0]);                                           //    Display info
      exit(0);                                                  //   Exit program
    }                                                           //  End switch
  }                                                             // End loop
  if (verbose>=2) {                                             // If high verbosity
    std::cout << "Bottom-tilted wavemaker:" << std::endl;       //  Title
    std::cout << "------------------------" << std::endl;       //  Underline
  }                                                             // End if
  int N = 51;                                                   // Problem size
  B3 b3;                                                        // Define case
  b3.N = N;                                                     // Fix size
  b3.dx = 0.04;                                                 // Grid spacing
  real_t L = 1.0;                                               // Wavemaker size
  real_t alpha = 0.05;                                          // Relative depth
  real_t h0 = alpha*L;                                          // Fix depth
  real_t t = 0.0;                                               // Current time
  real_t g = 9.81;                                              // Gravity
  real_t tmax = 5/sqrt(g*L*alpha)*L;                            // Maximum time
  real_t dt = tmax/400;                                         // Set timestep
  b3.eta.resize(N);                                             // Resize eta
  b3.phi_fs.resize(N);                                          // Resize potential
  b3.h.resize(N);                                               // Resize depth
  b3.ht.resize(N);                                              // Resize time-derivative
  for (int i=0; i<N; i++) {                                     // Loop over nodes
    real_t x = real_t(i)*b3.dx;                                 //  Get x-position
    b3.eta[i] = 0.0;                                            //  Initialize eta
    b3.phi_fs[i] = 0.0;                                         //  Initialize potential
    b3.h[i] = -h0;                                              //  Initialize depth
    b3.ht[i] = 0.0;                                             //  Initialize movement
  }                                                             // End if
  b3.Setup();                                                   // Setup preconditioner
  std::ofstream out_eta;                                        // Declare output
  out_eta.open("gauge.txt");                                    // Open file
  for (int nt=0; nt<400; nt++) {                                // Loop in time
    for (int i=0; i<N; i++) {                                   //  Loop over nodes
      real_t x = real_t(i)*b3.dx;                               //   Get x-position
      if (t<b*L/sqrt(g*h0)) {                                   //   If first part
	if (x<L) {                                              //    If flat part
	  b3.h[i] = -h0;                                        //     Fixed depth
	  b3.ht[i] = 0;                                         //     No motion
	} else {                                                //    Else
	  b3.h[i] = -h0-((x-L)/L)*(a*h0)*(1.0-t*sqrt(g*h0)/L/b);//     Set depth
	  b3.ht[i] = ((x-L)/L)*(a*h0)*(sqrt(g*h0)/L/b);         //     Set motion
	}                                                       //    End if
      } else {                                                  //   Else
	b3.h[i] = -h0;                                          //    Everything flat
	b3.ht[i] = 0;                                           //    No motion
      }                                                         //   End if
    }                                                           //  End loop
    b3.RK3(dt);                                                 //  Runge-Kutta step
    t += dt;                                                    //  Advance time
    if (b3.eta[25]/h0>maxeta) {                                 //  If larger eta
      maxeta = b3.eta[25]/h0;                                   //   Store in max eta
    }                                                           //  End if
    if (b3.eta[25]/h0<mineta) {                                 //  If smaller eta
      mineta = b3.eta[25]/h0;                                   //   Store in min eta
    }                                                           //  End if
#ifdef GUI
    x.a[nt] = t*sqrt(g*L*alpha)/L;                              //  Store x for plot
    y.a[nt] = b3.eta[25]/h0;                                    //  Store y for plot
#endif
    if (verbose>=2) {                                           //  If verbose
      std::cout << "time = " << t << std::endl;                 //   Print time
    }                                                           //  End if
    out_eta << t*sqrt(g*L*alpha)/L << " "
	    << b3.eta[25]/h0 << std::endl;                      //  Print elevation
  }                                                             // End loop
  out_eta.close();                                              // Close file
  std::cout << "Max. eta: " << maxeta << std::endl;             // 
  std::cout << "Min. eta: " << mineta << std::endl;             // Output
#ifdef GUI
  if (plot) {                                                   // If plotting
    mglQT gr(sample,"Bottom-mounted wavemaker");                //  Define window
    gr.Run();                                                   //  Plot results
  }                                                             // End if
#endif
}
