#include "b3.h"

//! TANDEM P01
int main() {
  int Mx = 10;                                                   // Set grid size
  int Mt = 2*Mx;                                                // CFL = Mx/Mt
  int N = 700*Mx+1;                                             // Set problem size
  B3 b3;                                                        // Define problem
  b3.P = 5; // set order of expansions
  b3.N = N;                                                     // Fix size
  b3.dx = 1.0/real_t(Mx);                                       // Grid spacing
  b3.eta.resize(N);                                             // Resize eta
  b3.phi_fs.resize(N);                                          // Resize potential
  b3.h.resize(N);                                               // Resize depth
  b3.ht.resize(N);                                              // Resize time-derivative
  std::ifstream file_eta;                                       // File for eta
  std::ifstream file_phi;                                       // File for phi
  file_eta.open("eta.txt");                                     // Open file
  file_phi.open("phi.txt");                                     // Open file
  for (int i=0; i<N; i++) {                                     // Loop over nodes
    real_t myeta;                                               //  Temp variable
    real_t myphi;                                               //  Temp variable
    file_eta >> myeta;
    b3.eta[i] = myeta;                                          //  Input eta
    file_phi >> myphi;
    b3.phi_fs[i] = myphi;                                       //  Input phi
    b3.h[i] = -1.0;                                             //  Constant depth
    b3.ht[i] = 0.0;                                             //  Fixed bottom
  }                                                             // End loop
  file_eta.close();                                             // Close file
  file_phi.close();                                             // Close file
  b3.Setup();                                                   // Setup preconditioner
  real_t t = 0.0;                                               // Current time
  real_t dt = sqrt(1.0/9.81)/real_t(Mt);                        // Fix imestep
  for (int nt=1; nt<=500*Mt; nt++) {                            // Loop in time
    b3.RK3(dt);                                                 //  Compute timestep
    t += dt;                                                    //  Advance time
    std::cout << "time = " << t << std::endl;                   //  Print status
    if (nt%(Mt*10) == 0) {                                      //  50 times per simu
      std::ofstream out;                                        //   Save final state
      std::string fname;                                        //   Declare file name
      char temp[5];                                             //   Temporary
      sprintf(temp,"%04d",nt/(Mt*10));                          //   Set file number
      fname = "out_"+std::string(temp)+".txt";                  //   Create file name
      out.open(fname.c_str());                                  //   Open file
      for (int i=0; i<N; i++) {                                 //   Loop over nodes
        real_t x = real_t(i)*b3.dx;                             //    Get x-position
        out << x << " " << b3.eta[i] << std::endl;              //    Save profile
      }                                                         //   End loop
      out.close();                                              //   Close file
    }                                                           //  End if
  }                                                             // End loop
  std::ofstream out;                                            // Save final state
  out.open("result.txt");                                       // Open file
  for (int i=0; i<N; i++) {                                     // Loop over nodes
    real_t x = real_t(i)*b3.dx;                                 //  Get x-position
    out << x << " " << b3.eta[i] << std::endl;                  //  Save profile
  }                                                             // End loop
  out.close();                                                  // Close file
}
