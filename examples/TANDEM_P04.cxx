#include "b3.h"

int main() {
  int N = 6000+1;                                               // Set problem size
  int ntmax = 72000;                                            // Number of timesteps
  B3 b3;                                                        // Define problem
  b3.P = 5;                                                     // Number of terms
  b3.N = N;                                                     // Fix size
  real_t tmax = 3600.0;                                         // Maximum time
  real_t dt = tmax/real_t(ntmax);                               // Fix timestep
  b3.dx = 30000.0/real_t(N-1);                                  // Grid spacing
  b3.eta.resize(N);                                             // Resize eta
  b3.phi_fs.resize(N);                                          // Resize potential
  b3.h.resize(N);                                               // Resize depth
  b3.ht.resize(N);                                              // Resize time-derivative
  for (int i=0; i<N; i++) {                                     // Loop over nodes
    real_t x = real_t(i)*b3.dx;                                 //  Get x-position
    if (x<1900.0) {                                             //  If initial part
      b3.eta[i] = 5.0;                                          //   Constant elevation
    } else if (x<2100.0) {                                      //  Else if transition
      real_t Delta = 229.756;                                   //   Define constant
      b3.eta[i] = 2.5*(1.0-tanh(Delta*(x/2000.0-1.0)));         //   Analytic formula
    } else {                                                    //  Else
      b3.eta[i] = 0.0;                                          //   Zero elevation
    }                                                           //  End if
    if (x<25000.0) {                                            //  If initial part
      b3.h[i] = -50.0;                                          //   Constant depth
    } else if (x<29000.0) {                                     //  Else if transition
      b3.h[i] = -50.0+(x-25000.0)/125.0;                        //   Constant slope
    } else {                                                    //  Else
      b3.h[i] = -18.0;                                          //   Constant depth
    }                                                           //  End if
    b3.ht[i] = 0.0;                                             //  Fixed bottom
    b3.phi_fs[i] = 0.0;                                         //  Zero potential
  }                                                             // End loop
  b3.Setup();                                                   // Setup preconditioner
  real_t t = 0.0;                                               // Current time
  std::ofstream out_runup;                                      // Define output
  out_runup.open("runup.txt");                                  // Open file
  for (int nt=1; nt<=ntmax; nt++) {                              // Loop in time
    b3.RK3(dt);                                                 //  Compute timestep
    t += dt;                                                    //  Advance time
    std::cout << "time = " << t << std::endl;                   //  Print status
    out_runup << t << " " << b3.eta[0]
	      << " " << b3.eta[N-1] << std::endl;               //  Print runup
    if (nt%(ntmax/240) == 0) {                                  //  If every 15s
      std::ofstream out;                                        //   Save final state
      std::string fname;                                        //   Declare file name
      char temp[5];                                             //   Temporary
      sprintf(temp,"%04d",nt/(ntmax/240));                      //   Set file number
      fname = "out_"+std::string(temp)+".txt";                  //   Create file name
      out.open(fname.c_str());                                  //   Open file
      for (int i=0; i<N; i++) {                                 //   Loop over nodes
	real_t x = real_t(i)*b3.dx;                             //    Get x-position
	out << x << " " << b3.eta[i] << std::endl;              //    Save profile
      }                                                         //   End loop
      out.close();                                              //   Close file
    }                                                           //  End if
  }                                                             // End loop
  out_runup.close();                                            // Close file
}
